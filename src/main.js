import 'd3'
import CompanyComponent from './modules/company'


Spider = window.Spider = function(source) {
    return function () {
        fetch(source).then(function(response) {
            return response.json()
          }).then(function(json) {
            let newCompany = new CompanyComponent(json.company);
            newCompany.drawComponent();
          }).catch(function(ex) {
            console.log('parsing failed', ex)
          })
    }(source)
}
