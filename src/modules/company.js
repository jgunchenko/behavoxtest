import 'd3'
import DepartmentComponent from './departments'

export default class CompanyComponent{
    constructor(data){
        this.canvas = this.getCanvas();
        this.data = data;
        this.open = false;
        this.departments = [];
    }

    getCanvas(){
        let canvas;
        let svg;
        let container = document.createElement('DIV');

        container.setAttribute('id', 'container');
        document.querySelector('body').style.margin = '0';
        document.querySelector('body').appendChild(container);
        container.style.position = 'relative';
        container.style.width = window.innerWidth + 'px';
        container.style.height = window.innerHeight + 'px';
        container.style.margin = '0';
        svg = d3.select('#container').append('svg')
        .attr('height', window.innerHeight)
        .attr('width', window.innerWidth);
        canvas = svg.append("g");
        canvas.attr('id', 'canvas')

        return canvas;
    }

    drawComponent() {
        if (!this.data) {
            return false;
        }
        let self = this;
        let visualComponent = this.canvas.append("g")
        let circle = visualComponent.append('circle')
        .attr("cx", window.innerWidth/2)
        .attr("cy", window.innerHeight/2)
        .attr("r", 50)
        .attr('fill', '#f2f2f2')
        .attr('stroke', '#2cabe1')
        .attr('stroke-width', '3');
        visualComponent.append('text')
        .attr('dx', window.innerWidth/2 - 45)
        .attr('dy', window.innerHeight/2)
        .attr('font-size', '14px')
        .text(self.data.name)


        visualComponent.on({
            'click': function() {
                if (self.data.hasOwnProperty('departments')) {
                    if (!self.open) {
                        if (self.departments.length === self.data.departments.length) {
                            self.departments.forEach(function(dep) {
                                dep.drawComponent();
                            })
                            self.open = true;
                        } else {
                            self.data.departments.forEach(function(dep, index, array) {
                                self.drawDepartment(dep, array.length, index);
                            })
                        }
                    } else {
                        self.departments.forEach(function(dep, index, array) {
                            self.cleanDepartment(dep);
                        })
                    }
                }
            }
        })
    }

    drawDepartment(data, length, index) {
        if (!data) {
            return false;
        }
        let outerRad = 200;
        let step = (2 * Math.PI)/length;
        let angle = index * step;

        data.coords = {
            cx: (window.innerWidth/2) + outerRad * Math.cos(angle),
            cy: (window.innerHeight/2) + outerRad * Math.sin(angle),
            x1: (window.innerWidth/2) + 50 * Math.cos(angle),
            y1: (window.innerHeight/2) + 50 * Math.sin(angle),
        }

        let depInstance = new DepartmentComponent(data, this.canvas);

        this.departments.push(depInstance);
        depInstance.drawComponent();
        this.open = true;
    }

    cleanDepartment(data) {
        if(this.canvas.selectAll('circle.employer').length){
            this.canvas.selectAll('circle.employer').remove();
            this.canvas.transition()
            .duration(750)
            .attr("transform", "translate(" + window.innerWidth / 2 + "," + window.innerHeight / 2 + ")scale(1)translate(" + -window.innerWidth/2 + "," + -window.innerHeight/2 + ")")
        }
        data.visualComponent.selectAll('text').remove();
        data.visualComponent.selectAll('circle').transition()
        .duration(1000)
        .attr('r',0);
        setTimeout(function(){
            data.visualComponent.selectAll('line').transition()
            .duration(1000)
            .attr({
                x2: data.coordinates.x1,
                y2: data.coordinates.y1
            })
        }, 900)
        setTimeout(function () {
            data.visualComponent.remove();
        }, 1500)
        this.open = false;
    }
}
