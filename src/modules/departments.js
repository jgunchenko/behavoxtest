import EmployerComponent from './employer'


export default class DepartmentComponent{

    constructor(data, canvas) {
        this.name = data.name;
        this.employers = data.employers;
        this.coordinates = data.coords;
        this.canvas = canvas;
        this.employersArr = [];
        this.visualComponent = null;
        this.active = null;
    }

    drawComponent(){
        let self = this;
        this.visualComponent = this.canvas.append('g');
        this.visualComponent.attr('class', 'department')
        this.visualComponent.append('line')
        .attr({
            x1: self.coordinates.x1,
            y1: self.coordinates.y1,
            x2: self.coordinates.x1,
            y2: self.coordinates.y1,
            'stroke-width': 2,
            stroke: '#2cabe1'
        })
        .transition()
        .duration(1000)
        .attr({
            x2: self.coordinates.cx,
            y2: self.coordinates.cy,
        });
        setTimeout(function(){
            let circle = self.visualComponent.append('circle')
            .attr('r', 0)
            .attr('cx', self.coordinates.cx)
            .attr('cy', self.coordinates.cy)
            .attr('fill', '#2cabe1');
            circle.on({
                'click': function () {
                    self.zoomComponent(this);
                    if (self.active !== null) {
                        if (self.employersArr.length === self.employers.length) {
                            self.employersArr.forEach(function(employer) {
                                employer.drawComponent();
                            })
                        } else {
                            self.employers.forEach(function(emp, index, array) {
                                self.showEmployers(emp,  index, array.length);
                            })
                        }
                    } else {
                        self.employersArr.forEach(function(emp, index, array) {
                            emp.visualComponent.remove();
                        })
                    }
                }
            })
            .transition()
            .duration(1500)
            .attr('r', 25);
        }, 900)
        setTimeout(function() {
            self.visualComponent.append('text')
            .attr('dx', self.coordinates.cx)
            .attr('dy', self.coordinates.cy - 30)
            .attr('font-size', '14px')
            .attr('text-ancor', 'middle')
            .text(self.name);
        }, 1500)
    }

    showEmployers(data, index, length) {
        let outerRad = 100 + 10*Math.random(10 - 5);
        let step = 3*(Math.PI)/length;
        let angle = index * step;

        data.coords = {
            cx: this.coordinates.cx + outerRad * Math.cos(angle),
            cy: this.coordinates.cy + outerRad * Math.sin(angle)
        }
        let empInstance = new EmployerComponent(data, this.name, this.canvas);
        this.employersArr.push(empInstance);
        empInstance.drawComponent();
    }

    zoomComponent(d) {
        let x, y, k;
        let width = window.innerWidth;
        let height = window.innerHeight;
        let g = this.visualComponent;

        if (d && this.active !== d) {
            x = this.coordinates.cx;
            y = this.coordinates.cy;
            k = 3;
            this.active = d;
        } else {
            x = width/2;
            y = height/2;
            k = 1;
            this.active = null;
        }

        this.canvas.transition()
        .duration(750)
        .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")scale(" + k + ")translate(" + -x + "," + -y + ")")
        .style("stroke-width", 1.5 / k + "px");
    }

    cleanEmployers(data) {
        data.visualComponent.selectAll('circle').transition()
        .duration(1000)
        .attr('r',0);
        setTimeout(function(){
            data.visualComponent.selectAll('line').transition()
            .duration(1000)
            .attr({
                x2: data.coordinates.x1,
                y2: data.coordinates.y1
            })
        }, 900)
        setTimeout(function () {
            data.visualComponent.remove();
        }, 1500)
        this.open = false;
    }
}
