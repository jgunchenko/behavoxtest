export default class EmployerComponent{

    constructor(data, department, canvas) {
        this.name = data.name;
        this.phone = data.phone;
        this.department = department;
        this.coordinates = data.coords;
        this.canvas = canvas;
        this.visualComponent = null;
    }

    drawComponent(){
        let self = this;
        this.visualComponent = this.canvas.append('circle')
            .attr('class', 'employer')
            .attr('r', 2)
            .attr('cx', self.coordinates.cx)
            .attr('cy', self.coordinates.cy)
            .attr('fill', '#2cabe1')
            .attr('stroke', '#ccc')
            .attr('stroke-width', '1')
            .on({
                'mouseover' : function () {
                    let infobubble = self.canvas.append('g')
                    .attr('class', 'employerinfo');
                    infobubble.append('rect')
                    .attr('rx', 1)
                    .attr('ry', 1)
                    .attr('x', self.coordinates.cx)
                    .attr('y', self.coordinates.cy - 45)
                    .attr("width", 70)
                    .attr("height", 30)
                    .attr('fill', '#fff')
                    .attr('stroke', '#000');
                    infobubble.append('text')
                    .attr('dx', self.coordinates.cx + 5)
                    .attr('dy', self.coordinates.cy - 36)
                    .attr('font-size', '6px')
                    .text('Name: ' + self.name )
                    infobubble.append('text')
                    .attr('dx', self.coordinates.cx + 5)
                    .attr('dy', self.coordinates.cy - 29)
                    .attr('font-size', '6px')
                    .text('Department: ' + self.department )
                    infobubble.append('text')
                    .attr('dx', self.coordinates.cx + 5)
                    .attr('dy', self.coordinates.cy - 22)
                    .attr('font-size', '6px')
                    .text('Phone: ' + self.phone )


                },
                'mouseout' : function () {
                    self.canvas.selectAll('g.employerinfo').remove();
                }
            })
    }
}
