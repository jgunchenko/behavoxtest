#Test task for Behavox

Used technologies:
* ES2015
* Babel
* Webpack
* D3
* whatwg fetch

To start application:
1. Open project folder in terminal
2. Input command: npm install; npm start
